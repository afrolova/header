var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var CleanPlugin = require('clean-webpack-plugin');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var isDev = JSON.parse(process.env.NODE_ENV !== 'production')

var plugins = [
    new CleanPlugin(['./dist'], { root: path.resolve(__dirname) }),
    new ExtractTextPlugin({
        filename: "header.[hash].css",
        disable: false,
        allChunks: true
    }),
    new HtmlWebpackPlugin({
        inject: 'body',
        template: './src/header.html'
    }),
    CopyWebpackPlugin([
        { from: './src/assets', to: './assets'},
    ]),
]

var rules = [
    {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
    },
]


var cssLoader = {
    test: /\.css$/,
    loaders:  ["style-loader", "css-loader"]
}

var lessLoader = {
    test: /\.less$/,
    loaders:  ["style-loader", "css-loader", "less-loader"]
}


if (isDev === false) {
    // for production
    plugins.push(new UglifyJSPlugin())
    
    cssLoader = {
        test: /\.css$/,
        loader:  ExtractTextPlugin.extract({
            use: ["css-loader"],
            publicPath: "/dist"
        })
    }

    lessLoader = {
        test: /\.less$/,
        loader:  ExtractTextPlugin.extract({
            use: ["css-loader", "autoprefixer-loader", "less-loader"],
            publicPath: "/dist"
        })
    }

}

rules.push(cssLoader)
rules.push(lessLoader)


module.exports = {
    resolve: {
        extensions: ['.js', '.css', '.less']
    },
    entry: './src/header.js',
    devtool: 'source-map',
    // cache: true,
    output: {
        path: path.join(__dirname, './dist' ),
        filename: 'header.[hash].js',
        publicPath: '/'
    },
    module: {
        rules: rules
    },
    plugins: plugins,
};
